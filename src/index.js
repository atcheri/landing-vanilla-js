import 'fullpage.js/vendors/scrolloverflow';
import fullpage from 'fullpage.js';

import { initTheme } from './scripts/theme';
import { initNav } from './scripts/nav';

import 'fullpage.js/dist/fullpage.css';

new fullpage('#fullPage', {
  autoScrolling: true,
  navigation: true,
  anchors: ['home', 'profile', 'projects', 'btw', 'contact'],
  navigationTooltips: ['Home', 'Profile', 'Projects', 'Misc', 'Contact'],
  showActiveTooltip: true,
  scrollOverflow: true
});

const devicons = document.querySelectorAll('.devicon');

devicons.forEach((i) => {
  i.addEventListener('mouseover', function () {
    this.classList.add('colored');
  });
  i.addEventListener('mouseleave', function () {
    this.classList.remove('colored');
  });
});

const storage = {
  getItem: (name) => {
    return localStorage.getItem(name);
  },
  setItem: (name, value) => {
    localStorage.setItem(name, value);
  }
};

initTheme({ storage });
initNav();

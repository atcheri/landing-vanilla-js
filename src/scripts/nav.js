const menu = document.querySelector('.menu-bar');
const nav = document.querySelector('nav');
const closeSpan = nav.querySelector('span.close');

export const initNav = () => {
  const toggle = (element, className = 'show') => {
    element.classList.toggle(className);
  };

  menu.addEventListener('click', () => {
    toggle(nav, 'show');
  });

  closeSpan.addEventListener('click', () => {
    toggle(nav, 'show');
  });
};

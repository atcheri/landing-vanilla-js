const themes = ['light', 'dark'];
const lightThemeChanger = document.querySelector('.theme-change.light');
const darkThemeChanger = document.querySelector('.theme-change.dark');

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const initTheme = ({
  storage = {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    getItem: () => {},
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setItem: () => {}
  }
}) => {
  const defaultTheme = storage.getItem('theme');
  if (defaultTheme) {
    changeBodyTheme(defaultTheme);
    const icon = document.querySelector(`.theme-change.${defaultTheme}`);
    icon.classList.add('active');
  }

  lightThemeChanger.addEventListener('click', function () {
    changeTheme(this, this.getAttribute('data-theme-type'));
  });

  darkThemeChanger.addEventListener('click', function () {
    changeTheme(this, this.getAttribute('data-theme-type'));
  });

  function changeTheme(element, theme) {
    document.querySelectorAll('.theme-change').forEach((el) => {
      el.classList.remove('active');
    });
    changeBodyTheme(theme);
    element.classList.add('active');
    storage.setItem('theme', theme);
  }
};

function changeBodyTheme(theme) {
  const body = document.querySelector('body');
  themes.forEach((theme) => {
    body.classList.remove(`theme-${theme}`);
  });
  body.classList.add(`theme-${theme}`);
}
